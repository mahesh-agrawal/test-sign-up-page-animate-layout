//
//  NewViewController.m
//  Test
//
//  Created by Mahesh Agrawal on 8/6/15.
//  Copyright (c) 2015 Mahesh Agrawal. All rights reserved.
//

#import "NewViewController.h"

@interface NewViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txt1;

@property (weak, nonatomic) IBOutlet UITextField *txt2;

@property (weak, nonatomic) IBOutlet UITextField *txt3;

@property (weak, nonatomic) IBOutlet UITextField *txt4;

@property (weak, nonatomic) IBOutlet UITextField *txt5;

@property (weak, nonatomic) IBOutlet UITextField *txt6;

@property (weak, nonatomic) IBOutlet UITextField *txt7;

@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (weak, nonatomic) IBOutlet UIView *viewControlContainer;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContainerWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintScrollBottom;

@property (nonatomic, strong) UITextField *txtCurrent;

@property (nonatomic, assign) CGSize currentKeyboardSize;

@end

@implementation NewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupKeyboard:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self setupKeyboard:NO];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.constraintContainerWidth.constant = self.view.frame.size.width;
    [self.view layoutIfNeeded];
}

#pragma mark - Keyboard Functions -

- (void)setupKeyboard:(BOOL)appearing
{
    if (appearing) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
        [self.view addGestureRecognizer:gestureRecognizer];
    }else{
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [UIView animateWithDuration:0.3 animations:^{
        self.constraintContainerWidth.constant = self.view.frame.size.height;
        [self.view layoutIfNeeded];
    }];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [UIView animateWithDuration:0.3 animations:^{
        self.constraintContainerWidth.constant = self.view.frame.size.width;
        [self.view layoutIfNeeded];
    }];
    
    if (fromInterfaceOrientation != UIInterfaceOrientationLandscapeLeft && fromInterfaceOrientation != UIInterfaceOrientationLandscapeRight) {
        [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.width * (self.view.frame.size.width / self.view.frame.size.height))];
    }
}

- (void)dismissKeyboard:(UIGestureRecognizer*)gestureRecognizer
{
    //resigning all text fields
    [self.txt1 resignFirstResponder];
    [self.txt2 resignFirstResponder];
    [self.txt3 resignFirstResponder];
    [self.txt4 resignFirstResponder];
    [self.txt5 resignFirstResponder];
    [self.txt6 resignFirstResponder];
    [self.txt7 resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    //getting the keyboard height
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    double duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    int curve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    [UIView beginAnimations:@"keyboardShown" context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    
    //custom code start
    self.currentKeyboardSize = keyboardSize;
    self.constraintScrollBottom.constant = keyboardSize.height;
    [self.view layoutIfNeeded];
    [self scrollToCurrentTextField];
    //custom code end
    
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    double duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    int curve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    [UIView beginAnimations:@"keyboardHidden" context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    
    //custom code start
    self.constraintScrollBottom.constant = 0;
    [self.view layoutIfNeeded];
    //custom code end
    
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}

- (void)scrollToCurrentTextField
{
    if (self.txtCurrent) {
        CGPoint offset= [self.txtCurrent convertRect:self.txtCurrent.bounds toView:self.scrollView].origin;
        
        if (UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])) {
            float expectedY = (self.view.frame.size.height - self.currentKeyboardSize.height) / 2.0;
            offset.y = offset.y - expectedY;
        }
        
        [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x, offset.y) animated:YES];
    }else{
        [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x, 0) animated:YES];
    }
}

#pragma mark - UITextFieldDelegate -

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.txtCurrent = textField;
    if (self.currentKeyboardSize.height>0) {
        [self scrollToCurrentTextField];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.txtCurrent = nil;
    [self scrollToCurrentTextField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
