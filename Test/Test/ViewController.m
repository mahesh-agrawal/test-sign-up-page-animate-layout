//
//  ViewController.m
//  SignUpPageTest
//
//  Created by Mahesh Agrawal on 7/10/15.
//  Copyright (c) 2015 Mahesh Agrawal. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txt1;

@property (weak, nonatomic) IBOutlet UITextField *txt2;

@property (weak, nonatomic) IBOutlet UITextField *txt3;

@property (weak, nonatomic) IBOutlet UITextField *txt4;

@property (weak, nonatomic) IBOutlet UITextField *txt5;

@property (weak, nonatomic) IBOutlet UITextField *txt6;

@property (weak, nonatomic) IBOutlet UITextField *txt7;

@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (weak, nonatomic) IBOutlet UIView *viewControlContainer;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContainerWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContainerTop;

@property (nonatomic, strong) UITextField *txtCurrent;

@property (nonatomic, assign) CGSize currentKeyboardSize;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupKeyboard:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self setupKeyboard:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //we are storing the current Y position of each text field inside its container
    self.txt1.tag = [self.txt1 convertRect:self.txt1.bounds toView:self.viewControlContainer].origin.y;
    self.txt2.tag = [self.txt2 convertRect:self.txt2.bounds toView:self.viewControlContainer].origin.y;
    self.txt3.tag = [self.txt3 convertRect:self.txt3.bounds toView:self.viewControlContainer].origin.y;
    self.txt4.tag = [self.txt4 convertRect:self.txt4.bounds toView:self.viewControlContainer].origin.y;
    self.txt5.tag = [self.txt5 convertRect:self.txt5.bounds toView:self.viewControlContainer].origin.y;
    self.txt6.tag = [self.txt6 convertRect:self.txt6.bounds toView:self.viewControlContainer].origin.y;
    self.txt7.tag = [self.txt7 convertRect:self.txt7.bounds toView:self.viewControlContainer].origin.y;
}


#pragma mark - Keyboard Functions -

- (void)setupKeyboard:(BOOL)appearing
{
    if (appearing) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
        [self.view addGestureRecognizer:gestureRecognizer];
    }else{
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [UIView animateWithDuration:0.3 animations:^{
        self.constraintContainerWidth.constant = self.view.frame.size.height;
        [self.view layoutIfNeeded];
    }];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [UIView animateWithDuration:0.3 animations:^{
        self.constraintContainerWidth.constant = self.view.frame.size.width;
        [self.view layoutIfNeeded];
    }];
    
    if (fromInterfaceOrientation != UIInterfaceOrientationLandscapeLeft && fromInterfaceOrientation != UIInterfaceOrientationLandscapeRight) {
        [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.width * (self.view.frame.size.width / self.view.frame.size.height))];
    }
}

- (void)dismissKeyboard:(UIGestureRecognizer*)gestureRecognizer
{
    //resigning all text fields
    [self.txt1 resignFirstResponder];
    [self.txt2 resignFirstResponder];
    [self.txt3 resignFirstResponder];
    [self.txt4 resignFirstResponder];
    [self.txt5 resignFirstResponder];
    [self.txt6 resignFirstResponder];
    [self.txt7 resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    //getting the keyboard height
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    double duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    int curve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    [UIView beginAnimations:@"keyboardShown" context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    
    //custom code start
    self.currentKeyboardSize = keyboardSize;
    self.image.alpha = 0.2;
    //custom code end
    
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
    
    [self scrollToCurrentTextField];
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    double duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    int curve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    [UIView beginAnimations:@"keyboardHidden" context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    
    //custom code start
    self.constraintContainerTop.constant = 0;
    self.image.alpha = 1.0;
    [self.viewContainer layoutIfNeeded];
    //custom code end
    
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}

- (void)scrollToCurrentTextField
{
    if (self.txtCurrent) {
        
        if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
            CGPoint offset= [self.txtCurrent convertRect:self.txtCurrent.bounds toView:self.scrollView].origin;
            [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x, offset.y) animated:YES];
        }else{
            [UIView animateWithDuration:0.3 animations:^{
                //we are changing the constraint to negetive value because we want to move the view up.
                //by this each text field will be shown at the position of the first text field.
                self.constraintContainerTop.constant = - self.txtCurrent.tag;
                [self.viewContainer layoutSubviews];
            }];
        }
    }
}

#pragma mark - UITextFieldDelegate -

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.txtCurrent = textField;
    if (self.currentKeyboardSize.height>0) {
        [self scrollToCurrentTextField];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
