//
//  main.m
//  Test
//
//  Created by Mahesh Agrawal on 7/10/15.
//  Copyright (c) 2015 Mahesh Agrawal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
